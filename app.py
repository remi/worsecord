import sqlite3, time, sys, os, flask, hashlib, json
import flask_socketio

dbVer = 0

sidToUser = {}

def getConnection():
    con = sqlite3.connect("test.sqlite")
    con.row_factory = sqlite3.Row
    return con

def createChannel(conn, type, name, desc = "Default description", category = "Default", icon = None):
    curs = conn.cursor()
    # type 0: text channel
    curs.execute("INSERT INTO Channels (ChannelType, ChannelName, ChannelDesc, ChannelCategory, Icon, Date) VALUES (0, ?, ?, ?, ?, ?)", (name, desc, category, icon, int(time.time())))
    curs.execute("SELECT ROWID,* FROM Channels WHERE rowId = LAST_INSERT_ROWID()")
    channel = curs.fetchone()
    print(f"channel created: {channel[2]} ({channel[0]})")
    curs.execute(f"CREATE TABLE ChannelText_{channel[0]} (UserId int NOT NULL, UserPersona int NOT NULL, MessageParams text, MessageBody text NOT NULL, MessageReply int, Date int, UsingKey int)")
    conn.commit()
    return True

def userPermitted(userId, channel):
    return True
    #return (channel["PermittedUsersRead"] == None and channel["PermittedUsersWrite"] == None) or (userId in channel["PermittedUsersRead"].split(",")) or (userId in channel["PermittedUsersWrite"].split(","))

def getChannels(conn, userId):
    curs = conn.cursor()
    curs.execute("SELECT ROWID,* FROM Channels")
    channels = curs.fetchall()
    chout = {}
    for channel in channels:
        if userPermitted(userId, channel):
            chout[channel["ROWID"]] = {}
            chout[channel["ROWID"]]["ChannelType"] = channel["ChannelType"]
            chout[channel["ROWID"]]["ChannelName"] = channel["ChannelName"]
            chout[channel["ROWID"]]["ChannelDesc"] = channel["ChannelDesc"]
            chout[channel["ROWID"]]["ChannelCategory"] = channel["ChannelCategory"]
            chout[channel["ROWID"]]["ID"] = channel["ROWID"]
            chout[channel["ROWID"]]["Icon"] = channel["Icon"]
            chout[channel["ROWID"]]["Date"] = channel["Date"]
    return chout

def getActiveChannels(conn, userId):
    channels = getChannels(conn, userId)
    curs = conn.cursor()
    chout = {}
    for channel in channels:
        date = 0
        try:
            curs.execute(f"SELECT ROWID,* FROM ChannelText_{channel} ORDER BY ROWID DESC LIMIT 1")
            msg = curs.fetchone()
            date = msg["Date"]
        except:
            date = channels[channel]["Date"]
        print(date)
        if (date > int(time.time()) - 604800):
            chout[channel] = channels[channel]
    return chout

def getPersonas(conn, userId):
    curs = conn.cursor()
    curs.execute(f"SELECT ROWID,* FROM UserPersonas_{userId}")
    personas = curs.fetchall()
    prout = {}
    for persona in personas:
        prout[persona["ROWID"]] = pData(persona, False)
    return prout

def createUser(conn, type, username, password):
    curs = conn.cursor()
    # type 0: regular user, type 1: sysop
    curs.execute("SELECT COUNT(*) FROM Users WHERE Username=?", (username,))
    if (curs.fetchone()[0] == 0):
        salt = os.urandom(32).hex()
        hashed = hashlib.sha256((password + salt).encode("utf-8")).hexdigest()
        curs.execute("INSERT INTO Users (Type, Username, PasswordHash, PasswordSalt, Date) VALUES (?, ?, ?, ?, ?)", (type, username, hashed, salt, int(time.time())))
        curs.execute("SELECT ROWID,* FROM Users WHERE rowId = LAST_INSERT_ROWID()")
        user = curs.fetchone()
        print(f"user created: {user[2]} ({user[0]})")
        curs.execute(f"CREATE TABLE UserPersonas_{user[0]} (PersonaName text NOT NULL, PersonaPfp text, PersonaDesc text, TextBG int NOT NULL DEFAULT 4294967295, TextFG int NOT NULL DEFAULT 0, UsernameFG int NOT NULL DEFAULT 0, TextFont text NOT NULL DEFAULT 'Arial', TextDecoration text NOT NULL DEFAULT 'unset', TextShadow text NOT NULL DEFAULT 'unset', Json text)")
        curs.execute(f"INSERT INTO UserPersonas_{user[0]} (PersonaName, PersonaPfp) VALUES (?,?)", (username,"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkYPj/HwADBwIAMCbHYQAAAABJRU5ErkJggg=="))
        curs.execute(f"SELECT ROWID,* FROM UserPersonas_{user[0]} WHERE rowId = LAST_INSERT_ROWID()")
        persona = curs.fetchone()
        print(f"persona created: {persona[1]} ({user[2]})")
        curs.execute(f"CREATE TABLE UserPings_{user[0]} (ChannelID int NOT NULL, UserID int NOT NULL)")
        conn.commit()
        return True
    else:
        return False

def logIn(conn, username, password):
    curs = conn.cursor()
    curs.execute("SELECT ROWID,* FROM Users WHERE Username=?", (username,))
    try:
        user = curs.fetchone()
        if (user[2] == username):
            if (hashlib.sha256((password + user[4]).encode("utf-8")).hexdigest() == user[3]):
                cookie = os.urandom(32).hex()
                curs.execute("INSERT INTO Sessions (User, Expire, Cookie) VALUES (?, ?, ?)", (user[0], int(time.time())+604800, cookie))
                conn.commit()
                return cookie
        return False
    except:
        return False # user not exist

def logInSession(conn, cookie): #convert this code to refer to it by string... PLEASE IT WOULD BE SO MUCH MORE MAINTAINABLE
    curs = conn.cursor()
    curs.execute("SELECT * FROM Sessions WHERE Cookie=?", (cookie,))
    try:
        user = curs.fetchone()
        if (user[1] < int(time.time())):
            return False
        return user[0]
    except:
        return False

def initDatabase():
    conn = getConnection()
    curs = conn.cursor()
    curs.execute("CREATE TABLE IF NOT EXISTS Channels (ChannelType int NOT NULL, ChannelName text NOT NULL, ChannelDesc text, ChannelCategory text, PermittedUsersRead text, PermittedUsersWrite text, Icon text, Date int)")
    curs.execute("SELECT Count(*) FROM Channels")
    if (curs.fetchone()[0] == 0):
        # new server, create a default channel
        print(createChannel(conn, 0, "home", "welcome to discord 2!"))
    curs.execute("CREATE TABLE IF NOT EXISTS Users (Type int NOT NULL, Username text NOT NULL, PasswordHash text NOT NULL, PasswordSalt text NOT NULL, Date int, Json text, Theme text DEFAULT '{}')")
    curs.execute("SELECT Count(*) FROM Users")
    if (curs.fetchone()[0] == 0):
        # and a default user
        print(createUser(conn, 1, "admin", "password")) #usually, do error checking to make sure we dont have dupe usernames... but that doesnt matter here cuz... yk... no users...
    curs.execute("CREATE TABLE IF NOT EXISTS Sessions (User int NOT NULL, Expire int NOT NULL, Cookie text NOT NULL)")
    curs.execute("CREATE TABLE IF NOT EXISTS Files (FileName text NOT NULL, Owner text, Type int)") #0: image, 1: image_lewd, 2: video, 3: video_lewd, 4: audio, 5: code, 6: binary, 7: emoji, 8: theme (basically json)
    #todo: delete dead sessions from here
    conn.close()

print("lading database...")
initDatabase()



app = flask.Flask(__name__)
socketio = flask_socketio.SocketIO(app)

def fourohone():
    return flask.abort(401, description="Sorry you need to be logged in :(((((")

def fourohoh():
    return flask.abort(400, description="Sorry you are bad at json :(((((")

def sess(cooki):
    conn = getConnection()
    user = None
    ok = True
    if (cooki != False):
        user = logInSession(conn, cooki)
        if (user == False):
            conn.close()
            ok = False 
    else:
        conn.close()
        ok = False
    return conn, user, ok

def pData(personaData, string = True):
    if (string):
        return json.dumps({
            "PersonaPfp": personaData["PersonaPfp"],
            "PersonaDesc": personaData["PersonaDesc"],
            "PersonaName": personaData["PersonaName"],
            "TextBG": personaData["TextBG"],
            "TextFG": personaData["TextFG"],
            "UsernameFG": personaData["UsernameFG"],
            "TextFont": personaData["TextFont"],
            "TextDecoration": personaData["TextDecoration"],
            "TextShadow": personaData["TextShadow"],
        })
    else:
        return {
            "PersonaPfp": personaData["PersonaPfp"],
            "PersonaDesc": personaData["PersonaDesc"],
            "PersonaName": personaData["PersonaName"],
            "TextBG": personaData["TextBG"],
            "TextFG": personaData["TextFG"],
            "UsernameFG": personaData["UsernameFG"],
            "TextFont": personaData["TextFont"],
            "TextDecoration": personaData["TextDecoration"],
            "TextShadow": personaData["TextShadow"],
        }

@app.route('/', methods=['GET', 'POST'])
def index():
    cooki = flask.request.cookies.get("seishun", False)
    if (cooki != False):
        conn = getConnection()
        if (logInSession(conn, cooki) != False):
            curs = conn.cursor()
            curs.execute("SELECT * FROM Users WHERE Username=?", (flask.request.form["username"],))
            theme = curs.fetchone()
            conn.close()
            if (theme["Theme"] != None):
                theme = theme["Theme"]
            else:
                theme = "{}"
            return flask.render_template('app.html.j2', theme=theme)
    if flask.request.method == 'GET':
        flask.g.message = "hi welcome to chillis"
        return flask.render_template('login.html.j2')
    if flask.request.method == 'POST':
        conn = getConnection()
        if (flask.request.form["type"] == "register"):
            if (createUser(conn, 0, flask.request.form["username"], flask.request.form["password"])):
                flask.g.message = "ok now log in"
            else:
                flask.g.message = "username taken (or ur dumb)"
        elif (flask.request.form["type"] == "login"):
            log = logIn(conn, flask.request.form["username"], flask.request.form["password"])
            if (log == False):
                flask.g.message = "wrong username/password"
            else:
                curs = conn.cursor()
                curs.execute("SELECT * FROM Users WHERE Username=?", (flask.request.form["username"],))
                theme = curs.fetchone()
                if (theme["Theme"] != None):
                    theme = theme["Theme"]
                else:
                    theme = "{}"
                conn.close()
                resp = flask.make_response(flask.render_template('app.html.j2', theme=theme))
                resp.set_cookie("seishun", log, int(time.time())+604800, int(time.time())+604800, "/")
                return resp
        else:
            flask.g.message = "hi welcome to chillis"
        conn.close()
        return flask.render_template('login.html.j2')

@app.route('/a/getPersonas')
def apiGetPersonas():
    conn, user, ok = sess(flask.request.cookies.get("seishun", False))
    if not ok: 
        conn.close()
        return fourohone()
    res = flask.jsonify(getPersonas(conn, user))
    conn.close()
    return res

@app.route('/a/getChannels')
def apiGetChannels():
    conn, user, ok = sess(flask.request.cookies.get("seishun", False))
    if not ok: 
        conn.close()
        return fourohone()
    res = flask.jsonify(getChannels(conn, user))
    conn.close()
    return res

@app.route('/a/getActiveChannels')
def apiGetActiveChannels():
    conn, user, ok = sess(flask.request.cookies.get("seishun", False))
    if not ok: 
        conn.close()
        return fourohone()
    res = flask.jsonify(getActiveChannels(conn, user))
    conn.close()
    return res

@app.route('/a/createChannel', methods=['POST'])
def apiCreateChannel():
    conn, user, ok = sess(flask.request.cookies.get("seishun", False))
    if not ok: 
        conn.close()
        return fourohone()
    jsn = flask.request.get_json()
    if (jsn.get("chname") == None):
        conn.close()
        return fourohoh()
    else:
        createChannel(conn, 0, jsn.get("chname"), jsn.get("chdesc", None), jsn.get("chcategory", None), jsn.get("chicon", None))
        conn.close()
        socketio.emit('channellist', 'uwu', to=f"authed")
        return "200"

@app.route('/a/setTheme', methods=['POST'])
def apiSetTheme():
    conn, user, ok = sess(flask.request.cookies.get("seishun", False))
    if not ok:
        conn.close()
        return fourohone()
    # todo: try catch this
    jsn = flask.request.get_json()
    curs = conn.cursor()
    curs.execute("UPDATE Users SET Theme=? WHERE ROWID=?", (jsn["theme"],user))
    conn.commit()
    conn.close()
    return "200"

@socketio.on('auth')
def siauth(token):
    conn, user, ok = sess(token)
    if ok:
        flask_socketio.join_room("authed")
        flask_socketio.join_room(f"user_{user}")
        sidToUser[flask.request.sid] = user
        flask_socketio.emit('welcome', f"hi, {user} :)")
    else:
        flask_socketio.emit('bye', 'auth token invalid. try logging in again?')
        flask_socketio.disconnect()
    conn.close()

@socketio.on('disconnect')
def sidisconnect():
    if('authed' in flask_socketio.rooms(flask.request.sid)):
        sidToUser.pop(flask.request.sid)

@socketio.on('message')
def simessage(channelId, persona, messagebody, messagereply, uuid):
    if('authed' in flask_socketio.rooms(flask.request.sid)):
        # try:
        user = sidToUser[flask.request.sid]
        conn = getConnection()
        curs = conn.cursor()
        curs.execute("SELECT * FROM Channels WHERE ROWID=?", (channelId,))
        channel = curs.fetchone() #wtf is "messageparams"
        if userPermitted(user, channel): #UserId int, UserPersona int, MessageParams text, MessageBody text, MessageReply int, Date int, UsingKey int
            timestamp = int(time.time())
            curs.execute(f"SELECT ROWID,* FROM ChannelText_{channelId} ORDER BY ROWID DESC LIMIT 1") # ????????
            lastmsg = curs.fetchone()
            if (lastmsg != None and int(lastmsg["UserId"]) == int(user) and int(lastmsg["UserPersona"]) == int(persona)):
                curs.execute(f"UPDATE ChannelText_{channelId} SET MessageBody=? WHERE ROWID=?", (lastmsg["MessageBody"]+messagebody+"<br>", lastmsg["ROWID"]))
                curs.execute(f"UPDATE ChannelText_{channelId} SET Date=? WHERE ROWID=?", (int(time.time()), lastmsg["ROWID"]))
                conn.commit()
                conn.close()
                flask_socketio.emit('edit', (channelId, lastmsg["ROWID"], lastmsg['MessageBody']+"\n"+messagebody), to=f"ChannelText_{channelId}")
                flask_socketio.emit('sent', (uuid, lastmsg["Date"]), to=f"user_{user}")
            else:
                curs.execute(f"INSERT INTO ChannelText_{channelId} (UserId, UserPersona, MessageBody, MessageReply, Date) VALUES (?, ?, ?, ?, ?)", (user, persona, messagebody+"<br>", None, timestamp))
                conn.commit()
                curs.execute(f"SELECT * FROM UserPersonas_{int(user)} WHERE ROWID=?", (persona,))
                personaData = curs.fetchone()
                conn.close()
                flask_socketio.emit('msg', (channelId, user, persona, pData(personaData), messagebody, None, curs.lastrowid, timestamp), to=f"ChannelText_{channelId}")
                flask_socketio.emit('sent', (uuid, timestamp), to=f"user_{user}")
        # except Exception as e:
            # print(e)
            # conn.close()
            # flask_socketio.emit('notsent', uuid, to=f"user_{user}")
    else:
        conn.close()
        flask_socketio.emit('notsent', uuid, to=f"user_{user}")

@socketio.on('channel')
def sijoinchannel(channelId):
    if('authed' in flask_socketio.rooms(flask.request.sid)):
        user = sidToUser[flask.request.sid]
        conn = getConnection()
        curs = conn.cursor()
        curs.execute("SELECT * FROM Channels WHERE ROWID=?", (channelId,))
        channel = curs.fetchone()
        if userPermitted(user, channel):
            flask_socketio.join_room(f"ChannelText_{channelId}")
            # read last 20 messages, send them
            curs.execute(f"SELECT ROWID,* FROM ChannelText_{channelId} ORDER BY ROWID DESC LIMIT 50")
            msgs = curs.fetchall()
            for msg in msgs:
                curs.execute(f"SELECT * FROM UserPersonas_{int(msg['UserId'])} WHERE ROWID=?", (msg["UserPersona"],))
                personaData = curs.fetchone()
                flask_socketio.emit('msg', (channelId, msg["UserId"], msg["UserPersona"], pData(personaData), msg["MessageBody"], msg["MessageReply"], msg["ROWID"], msg["Date"]))
            conn.close()
        else:
            conn.close()


if __name__ == '__main__':
    socketio.run(app, host="localhost")