let main = document.querySelector("main");
let mainTop = main.querySelector("#mainTop");
let mainMessages = main.querySelector("#mainMessages");
var mainMessagesContainer = main.querySelector("#mainMessagesContainer")
let mainBottom = main.querySelector("#mainBottom");
let messageBox = mainBottom.querySelector("textarea");

let nav = document.querySelector("nav");
let navStatic = nav.querySelector("#navStatic");
let navChannels = nav.querySelector("#navChannels");

let aside = document.querySelector("aside");

var socket;

var currentChannel = -1;

var personas = {};

var notsenttimeout;

function updateChannels() {
    fetch("/a/getActiveChannels").then((res) => res.json()).then((res) => {
        ncOld = document.createElement("swag");
        ncOld.innerHTML = navChannels.innerHTML;
        navChannels.innerHTML = "";
        var ch = {}
        Object.values(res).forEach(channel => {
            var chcat = channel["ChannelCategory"] ?? "Default";
            if (ch[chcat] == undefined) {
                var category = document.createElement("details");
                category.dataset.name = chcat;
                category.dataset.longPressDelay = "500";
                category.setAttribute("open", true);
                if (ncOld.querySelector(".navCategory[data-name='"+chcat+"']")) {
                    if (!ncOld.querySelector(".navCategory[data-name='"+chcat+"']").getAttribute("open")) {
                        category.removeAttribute("open");
                    }
                }
                category.classList.add("navCategory");
                var categoryHeader = document.createElement("summary");
                categoryHeader.addEventListener('long-press', (e) => {
                    e.preventDefault();
                    showOverlay("categoryEdit");
                })
                categoryHeader.addEventListener('contextmenu', (e) => {
                    e.preventDefault();
                    showOverlay("categoryEdit");
                })
                categoryHeader.classList.add("navItem");
                categoryHeader.innerText = chcat;
                category.appendChild(categoryHeader);
                ch[chcat] = category;
            }
            var chdiv = document.createElement("div");
            chdiv.classList.add("navItem");
            chdiv.innerHTML = channel["ChannelName"];
            chdiv.id = "channelId" + channel["ID"];
            chdiv.onclick = () => {
                joinChannel(channel["ID"])
            }
            chdiv.addEventListener('long-press', (e) => {
                e.preventDefault();
                showOverlay("channelEdit");
            })
            chdiv.addEventListener('contextmenu', (e) => {
                e.preventDefault();
                showOverlay("channelEdit");
            })
            ch[chcat].appendChild(chdiv);
        });
        Object.keys(ch).forEach(category => {
            navChannels.appendChild(ch[category]);
        })
        if (ncOld.querySelector(".navItem.active")) {
            document.querySelector("#"+ncOld.querySelector(".navItem.active").id).classList.add("active");
        }
    })
}

function joinChannel(id) {
    currentChannel = id;
    document.querySelectorAll(".navItem").forEach(navItem => {navItem.classList.remove("active")});
    document.querySelector("#channelId" + id).classList.add("active");
    mainMessages.innerHTML = "";
    socket.emit("channel", id);
}

function showOverlay(div) {
    hideOverlay();
    overlay = document.querySelector("overlay#" + div);
    overlayInner = overlay.querySelector("div");
    overlay.onclick = (e) => {
        if (!e.composedPath().includes(overlayInner)) {
            overlay.classList.remove("show");
        }
    }
    overlay.classList.add("show");
}

function hideOverlay(div = null) {
    if (div == null) {
        document.querySelectorAll("overlay").forEach(overlay => {overlay.classList.remove("show");})
    } else {
        document.querySelector("overlay#" + div).classList.remove("show");
    }
}

//OVERLAY: create channel

function createChannel() {
    overlay = document.querySelector("overlay#createChannel div");
    chinfo = overlay.querySelectorAll("input");
    button = overlay.querySelector("button");

    button.setAttribute("disabled", "");

    if (chinfo[0].value == "") {
        alert("dumass");
        return;
    }

    data = {"chname": chinfo[0].value, "chdesc": chinfo[1].value == "" ? null : chinfo[1].value, "chcategory": chinfo[2].value == "" ? null : chinfo[2].value};

    fetch('/a/createChannel', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        button.disabled = false;
        if (response.ok) {
            hideOverlay("createChannel");
            updateChannels();
            //functoin that loads a channel here
        } else {
            alert('Error: ' + error);
        }
    })
    .catch((error) => {
        button.disabled = false;
        alert('Error: ' + error);
    });
}

// INPUT TEXTBOX THINGY
function sendMessage(channelId, persona, messagebody, messagereply) {
    messagebody.replace(/\n/g, "<br />");
    socket.emit("message", channelId, persona, messagebody, messagereply, "asdf")
    mainMessagesContainer.scrollTop = mainMessagesContainer.scrollHeight;
    setTimeout(() => {
        mainMessagesContainer.scrollTop = mainMessagesContainer.scrollHeight;
    }, 10);
}

messageBox.addEventListener("keydown", (e) => {
    if (e.keyCode == 13) {
        if (!e.shiftKey)  {
            e.preventDefault();
            sendMessage(currentChannel, "1", mainBottom.querySelector("textarea").value, null);
            messageBox.disabled = true;
        }
    }
});

function personaOverlayAdd(persona, container, key) {
    persona = persona ?? {"PersonaName": "New persona", "PersonaDesc": "blah uwu", "TextBG": 4294967295, "TextFG": 0, "UsernameFG": 4294967295, "TextFont": "Arial", "TextDecoration": "unset", "TextShadow": "unset"};
    pContainer = document.createElement("div");
    container.appendChild(pContainer);
    pContainer.id = "persona_" + key;
    pContainer.innerHTML = `
        <p contentEditable="true">${persona.PersonaName}</p>
        <div>
        description: <input type="text" name="desc" value="${persona.PersonaDesc}"><br>
        text bg: <input type="text" name="textbg" value="${"#" + persona.TextBG.toString(16).padStart(6, '0')}"><br>
        text fg: <input type="text" name="textfg" value="${"#" + persona.TextFG.toString(16).padStart(6, '0')}"><br>
        username fg: <input type="text" name="usernamefg" value="${"#" + persona.UsernameFG.toString(16).padStart(6, '0')}"><br>
        text font: <input type="text" name="textfont" value="${persona.TextFont}"><br>
        text decoration: <input type="text" name="textdecoration" value="${persona.TextDecoration}"><br>
        text shadow: <input type="text" name="textshadow" value="${persona.TextShadow}"><br>
        </div><br>
    `;
}

//persona overlay
function personaOverlay() {
    let overlay = document.querySelector("#personas");
    let overlayMain = overlay.querySelector("main")
    let overlayLoader = overlay.querySelector(".lds-ripple");
    overlayMain.style.display = "none";
    overlayLoader.style.display = "block";
    let add = document.createElement("button");
    add.innerHTML = "add";
    add.onclick = () => {personaOverlayAdd(null, overlayMain, -1)};
    overlayMain.appendChild(add);
    let save = document.createElement("button");
    save.innerHTML = "save";
    save.onclick = () => {alert("nah")};
    overlayMain.appendChild(save);
    fetch("/a/getPersonas").then(res => res.json()).then(res => {
        Object.keys(res).forEach(persona => {
            personaOverlayAdd(res[persona], overlayMain, persona)
        })
        overlayMain.style.display = "block";
        overlayLoader.style.display = "none";    
    })
}

//persona update
function updatePersona(user, personaId, personaData) {
    //todo :3
}

//message recieve???
function recieveMessage(user, personaId, personaData, messagebody, messagereply, uuid, timestamp) {
    var isBottom = mainMessagesContainer.scrollTop + mainMessagesContainer.offsetHeight === mainMessagesContainer.scrollHeight;
    var message = document.createElement("div");
    message.classList.add("message");
    message.dataset.user = user;
    message.dataset.persona = personaId;
    message.dataset.id = uuid; 
    message.style.order = uuid;
    personaData = JSON.parse(personaData);
    if (personas[user] == undefined || personas[user][personaId] == undefined) {
        if (personas[user] == undefined) personas[user] = {};
        personas[user][personaId] = personaData;
    } else if (personas[user][personaId] != personaData) {
        personas[user][personaId] = personaData;
        updatePersona(user, personaId);
    }
    message.style.color = "#" + personaData["TextFG"].toString(16).padStart(6, '0');
    message.style.background = "#" + personaData["TextBG"].toString(16).padStart(6, '0');
    message.style.fontFamily = personaData["TextFont"] + " !important";
    var messageContent = document.createElement("div");
    var messageHeader = document.createElement("div");
    messageHeader.classList.add("messageHeader");
    var messagePfp = document.createElement("img");
    messagePfp.src = personaData["PersonaPfp"];
    messageHeader.appendChild(messagePfp);
    messageHeaderMain = document.createElement("div");
    var messageHeaderUsername = document.createElement("p");
    messageHeaderUsername.innerText = personaData["PersonaName"];
    messageHeaderUsername.style.color = "#" + personaData["TextFG"].toString(16).padStart(6, '0');
    var messageHeaderTimestamp = document.createElement("p");
    var d = new Date(timestamp);
    messageHeaderTimestamp.innerText = d.toLocaleTimeString();
    messageHeaderTimestamp.style.color = "#" + personaData["TextFG"].toString(16).padStart(6, '0') + "C0";
    messageHeaderMain.appendChild(messageHeaderUsername);
    messageHeaderMain.appendChild(messageHeaderTimestamp);
    messageHeaderMain.classList.add("messageHeaderMain");
    messageHeaderMain.style.textDecoration = personaData["TextDecoration"];
    messageHeaderMain.style.textShadow = personaData["TextShadow"];
    messageHeader.appendChild(messageHeaderMain);
    var messageText = document.createElement("div");
    messageText.innerHTML = messagebody;
    messageText.classList.add("messageText")
    messageContent.appendChild(messageHeader);
    messageContent.appendChild(messageText);
    messageContent.classList.add("messageContent");
    messageContent.style.textDecoration = personaData["TextDecoration"];
    messageContent.style.textShadow = personaData["TextShadow"];
    var messageActions = document.createElement("div");
    messageActions.innerHTML = "deltett";
    messageActions.classList.add("messageActions");
    messageHeader.appendChild(messageActions)
    message.appendChild(messageHeader);
    message.appendChild(messageContent);
    mainMessages.appendChild(message);
    if (isBottom) {
        mainMessagesContainer.scrollTop = mainMessagesContainer.scrollHeight;
        setTimeout(() => {
            mainMessagesContainer.scrollTop = mainMessagesContainer.scrollHeight;
        }, 10);
    }
}

function editMessage(uuid, messageBody) {
    document.querySelector(".message[data-id='"+uuid+"'] div.messageText").innerHTML = messageBody;
}

// LOAD THEME
function loadTheme(theme) {
    let keys = Object.keys(theme);
    let values = Object.values(theme);
    for (var x = 0; x < Object.keys(theme).length; x++) {
        document.documentElement.style.setProperty("--" + keys[x], values[x]);
    }
}

// SAVE THEME
function saveTheme(text) {
    data = {"theme": text}

    fetch('/a/setTheme', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        if (response.ok) {
            alert("nice")
        } else {
            alert('Error: ' + error);
        }
    })
    .catch((error) => {
        alert('Error: ' + error);
    });
}

window.onload = () => {
    try {
        loadTheme(theme);
    } catch (e) {}
    updateChannels();
    socket = io();
    socket.on("connect", () => {
        socket.emit("auth", getCookie('seishun'));
    })
    socket.on("welcome", () => {
        document.querySelector("loading").style.opacity = "0";
        document.querySelector("loading").style.pointerEvents = "unset";
        updateChannels();
        if (currentChannel != -1) {
            joinChannel(currentChannel); /* reload the current channel on lost connection */
        }
        setTimeout(() => {
            document.querySelector("loading").style.display = "none";
        }, 300);
    })
    socket.on("bye", () => {
        window.location.replace('/');
    })
    socket.on("disconnect", () => {
        document.querySelector("loading").style.display = "block";
        document.querySelector("loading").style.opacity = "1";
        document.querySelector("loading").style.pointerEvents = "none";
    })
    socket.on("msg", (channelId, user, personaId, personaData, messagebody, messagereply, uuid, timestamp) => {
       if (channelId == currentChannel) recieveMessage(user, personaId, personaData, messagebody, messagereply, uuid, timestamp);
    })
    socket.on("edit", (channelId, uuid, messagebody) => {
        if (channelId == currentChannel) editMessage(uuid, messagebody);
    })
    socket.on("sent", () => {
        messageBox.disabled = false;
        messageBox.value = "";
    })
    socket.on("notsent", () => {
        messageBox.disabled = false;
        messageBox.style.borderColor = "var(--error)";
        clearTimeout(notsenttimeout);
        notsenttimeout = setTimeout(() => {
            messageBox.style.borderColor = "unset";
        }, 1000)
    })
    socket.on("channellist", () => {
        updateChannels();
    })
}